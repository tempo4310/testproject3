const express = require('express')
const db = require('../db')
const multer = require('multer')

const upload = multer({ dest: 'uploads' })

const router = express.Router()

router.post(
  '/upload-image/:id',
  upload.single('photo'),
  (request, response) => {
    const { id } = request.params

    const fileName = request.file.filename

    const statement = `UPDATE carTB 
        SET carImage = ?
        WHERE id = ?
      `
    db.pool.query(statement, [fileName, id], (error, data) => {
      const result = {}
      if (error) {
        result['status'] = 'error'
        result['error'] = error
      } else {
        result['status'] = 'success'
        result['data'] = data
        response.send(result)
      }
    })
  }
)

router.post('/add', (request, response) => {
  const { id, companyName, model, price } = request.body
  const statement = `
        INSERT INTO carTB(id, companyName, model, price)
        VALUES (?, ?, ?, ?);     
        `
  db.pool.query(statement, [id, companyName, model, price], (error, data) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      result['status'] = 'success'
      result['data'] = data
      response.send(result)
    }
  })
})

router.get('/showcars', (request, response) => {
  const statement = ` SELECT * 
                      FROM carTB 
                      ORDER BY id;
                    `
  db.pool.query(statement, (error, data) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      result['status'] = 'success'
      result['data'] = data
    }
    response.send(result)
  })
})

router.delete('/:id', (request, response) => {
  const { id } = request.params

  console.log(id)

  const statement = ` 
        DELETE FROM carTB 
        WHERE id = ?;`
  db.pool.query(statement, [id], (error, data) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      result['status'] = 'success'
    }
    response.send(result)
  })
})

module.exports = router
