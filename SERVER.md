# server.js

```js
const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.json())
app.use(cors())

const userRouter = require('./routes/user')
const carRouter = require('./routes/car')

app.use('/user', userRouter)
app.use('/car', carRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server startedd on port 4000')
})
```

# db.js

```js
const mysql = require('mysql')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'manager',
  database: 'labExam_tempo',
  port: 3306,
  connectionLimit: 20,
})

module.exports = {
  pool,
}
```

# routes/user.js

```js
const express = require('express')
const db = require('../db')

const router = express.Router()

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `
        SELECT empid, companyName, email 
        FROM userTB 
        WHERE email = ? AND password = ?
        `
  db.pool.query(statement, [email, password], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else if (users.length === 0) {
      result['status'] = 'error'
      result['error'] = 'user does not exist'
    } else {
      const user = users[0]
      result['status'] = 'success'
      result['data'] = {
        companyName: user['companyName'],
        email: user['email'],
      }
    }
    response.send(result)
  })
})

module.exports = router
```

# routes/car.js

```js
const express = require('express')
const db = require('../db')

const router = express.Router()

router.post('/add', (request, response) => {
  const { id, companyName, model, price, carImage } = request.body
  const statement = `
        INSERT INTO carTB(id, companyName, model, price, carImage)
        VALUES (?, ?, ?, ?, ?);     
        `
  db.pool.query(
    statement,
    [id, companyName, model, price, carImage],
    (error, data) => {
      const result = {}
      if (error) {
        result['status'] = 'error'
        result['error'] = error
      } else {
        result['status'] = 'success'
        result['data'] = data
        response.send(result)
      }
    }
  )
})

router.get('/showcars', (request, response) => {
  const statement = ` SELECT * 
                      FROM carTB 
                      ORDER BY id;
                    `
  db.pool.query(statement, (error, data) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      result['status'] = 'success'
      result['data'] = data
    }
    response.send(result)
  })
})

router.delete('/:id', (request, response) => {
  const { id } = request.params

  console.log(id)

  const statement = ` 
        DELETE FROM carTB 
        WHERE id = ?;`
  db.pool.query(statement, [id], (error, data) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      result['status'] = 'success'
      result['data'] = data
    }
    response.send(result)
  })
})

module.exports = router
```

# Dependencies:

```bash
#Basic
yarn add mysql express cors nodemon

#Adavnced
yarn add mysql mysql2 express cors nodemon crypto-js jsonwebtoken
```
