const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.json())
app.use(cors())

app.use(express.static('uploads'))

const userRouter = require('./routes/user')
const carRouter = require('./routes/car')

app.use('/user', userRouter)
app.use('/car', carRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server startedd on port 4000')
})
