const express = require('express')
const db = require('../db')

const router = express.Router()

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `
        SELECT empid, companyName, email 
        FROM userTB 
        WHERE email = ? AND password = ?
        `
  db.pool.query(statement, [email, password], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else if (users.length === 0) {
      result['status'] = 'error'
      result['error'] = 'user does not exist'
    } else {
      const user = users[0]
      result['status'] = 'success'
      result['data'] = {
        companyName: user['companyName'],
        email: user['email'],
      }
    }
    response.send(result)
  })
})

module.exports = router
